package br.com.digio.teste.model;

import org.springframework.data.annotation.Id;

import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.UUID;

public class LancamentoContabil  implements Serializable {

    @Id
    @GeneratedValue(generator = "UUID")
    private UUID id;

    private int contaContabil;

    private String data;

    private double valor;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public int getContaContabil() {
        return contaContabil;
    }

    public void setContaContabil(int contaContabil) {
        this.contaContabil = contaContabil;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
}
