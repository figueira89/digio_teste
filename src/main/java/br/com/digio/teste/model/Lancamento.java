package br.com.digio.teste.model;

import java.io.Serializable;

public class Lancamento implements Serializable {

    private int contaContabil;

    private String data;

    private double valor;

    public int getContaContabil() {
        return contaContabil;
    }

    public void setContaContabil(int contaContabil) {
        this.contaContabil = contaContabil;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
}
