package br.com.digio.teste.controller;

import br.com.digio.teste.model.LancamentoContabil;
import br.com.digio.teste.service.LancamentoContabilService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
public class LancamentoContabilController {

    @Autowired
    public LancamentoContabilService service;


    @PostMapping(value = "/lancamentos-contabeis", consumes = "application/json", produces = "application/json")
    ResponseEntity<?> insert(@RequestBody LancamentoContabil lancamento) {
        return service.insert(lancamento.getContaContabil(), lancamento.getData(), lancamento.getValor());
    }

    @GetMapping(value = "/lancamentos-contabeis/{id}")
    ResponseEntity<?> getById(@PathVariable("id") UUID id) {
        return service.getLancamentoById(id);
    }

    @GetMapping(value = "/lancamentos-contabeis")
    ResponseEntity<?> getByAccount(@RequestParam("contaContabil") int conta) {
        return service.getLancamentoByConta(conta);
    }

    @GetMapping(value = "/lancamentos-contabeis/stats")
    ResponseEntity<?> getAllStats() {
        return service.getAllStats();
    }

    @GetMapping(value = "/lancamentos-contabeis/stats/")
    ResponseEntity<?> getAllStatsByAccount(@RequestParam("contaContabil") int conta) {
        return service.getAllStatsByAccount(conta);
    }

}
