package br.com.digio.teste.service;

import br.com.digio.teste.model.Lancamento;
import br.com.digio.teste.model.LancamentoStats;
import br.com.digio.teste.repository.LancamentoContabilRepository;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Service
public class LancamentoContabilService {

    @Autowired
    LancamentoContabilRepository repository;


    public ResponseEntity<?> insert(int conta, String data, double valor){
        UUID uuid = null;
        uuid = repository.insert(conta, data, valor);

        if (uuid == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Erro ao incluir o Lançamento Contábil");
        }

        JSONObject json = new JSONObject();
        json.put("id", uuid);

        return ResponseEntity.status(HttpStatus.CREATED).body(json.toString());
    }

    public ResponseEntity<?> getLancamentoById(UUID id){
        Lancamento lancamento = repository.getLancamentoById(id);
        if (Objects.isNull(lancamento)) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Registro contábil não localizado.");
        }

        return ResponseEntity.status(HttpStatus.OK).body(lancamento);
    }

    public ResponseEntity<?> getLancamentoByConta(int conta){
        List<Lancamento> lancamento = repository.getLancamentoByConta(conta);

        if (ObjectUtils.isEmpty(lancamento) || Objects.isNull(lancamento) || lancamento.size() == 0) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Registros contábeis não localizados.");
        }

        return ResponseEntity.status(HttpStatus.OK).body(lancamento);
    }

    public ResponseEntity<?> getAllStats(){
        LancamentoStats lancamento = repository.getAllStats();

        if (lancamento.getQuantidade() == 0) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Registro não localizado.");
        }

        return ResponseEntity.status(HttpStatus.OK).body(lancamento);
    }

    public ResponseEntity<?> getAllStatsByAccount(int conta){
        LancamentoStats lancamento = repository.getAllStatsByAccount(conta);

        if (lancamento.getQuantidade() == 0) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Registro não localizado.");
        }

        return ResponseEntity.status(HttpStatus.OK).body(lancamento);
    }
}
