package br.com.digio.teste.mapper;

import br.com.digio.teste.model.LancamentoContabil;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

public class LancamentoContabilRowMapper implements RowMapper<LancamentoContabil> {
    @Override
    public LancamentoContabil mapRow(ResultSet resultSet, int i) throws SQLException {
        LancamentoContabil lancamento = new LancamentoContabil();
        lancamento.setId(UUID.fromString(resultSet.getString("id")));
        lancamento.setContaContabil(resultSet.getInt("conta_contabil"));
        lancamento.setData(resultSet.getString("data"));
        lancamento.setValor(resultSet.getDouble("valor"));
        return lancamento;
    }
}
