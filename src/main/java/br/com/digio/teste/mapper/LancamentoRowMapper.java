package br.com.digio.teste.mapper;

import br.com.digio.teste.model.Lancamento;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class LancamentoRowMapper implements RowMapper<Lancamento> {
    @Override
    public Lancamento mapRow(ResultSet resultSet, int i) throws SQLException {
        Lancamento lancamento = new Lancamento();
        lancamento.setContaContabil(resultSet.getInt("conta_contabil"));
        lancamento.setData(resultSet.getString("data"));
        lancamento.setValor(resultSet.getDouble("valor"));
        return lancamento;
    }
}
