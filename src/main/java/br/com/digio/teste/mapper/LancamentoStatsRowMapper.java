package br.com.digio.teste.mapper;

import br.com.digio.teste.model.LancamentoContabil;
import br.com.digio.teste.model.LancamentoStats;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

public class LancamentoStatsRowMapper implements RowMapper<LancamentoStats> {
    @Override
    public LancamentoStats mapRow(ResultSet resultSet, int i) throws SQLException {
        LancamentoStats lancamento = new LancamentoStats();
        lancamento.setSoma(resultSet.getDouble("soma"));
        lancamento.setMinimo(resultSet.getDouble("minimo"));
        lancamento.setMaximo(resultSet.getDouble("maximo"));
        lancamento.setMedia(resultSet.getDouble("media"));
        lancamento.setQuantidade(resultSet.getInt("quantidade"));
        return lancamento;
    }
}
