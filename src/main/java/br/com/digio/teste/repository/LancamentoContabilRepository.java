package br.com.digio.teste.repository;

import br.com.digio.teste.model.Lancamento;
import br.com.digio.teste.model.LancamentoContabil;
import br.com.digio.teste.model.LancamentoStats;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface LancamentoContabilRepository {

    UUID insert(int conta, String data, double valor);

    Lancamento getLancamentoById(UUID id);

    List<Lancamento> getLancamentoByConta(int conta);

    LancamentoStats getAllStats();

    LancamentoStats getAllStatsByAccount(int conta);

}
