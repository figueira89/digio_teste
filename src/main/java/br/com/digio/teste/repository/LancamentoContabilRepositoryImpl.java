package br.com.digio.teste.repository;

import br.com.digio.teste.mapper.LancamentoContabilRowMapper;
import br.com.digio.teste.mapper.LancamentoRowMapper;
import br.com.digio.teste.mapper.LancamentoStatsRowMapper;
import br.com.digio.teste.model.Lancamento;
import br.com.digio.teste.model.LancamentoContabil;
import br.com.digio.teste.model.LancamentoStats;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

public class LancamentoContabilRepositoryImpl implements LancamentoContabilRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public UUID insert(int conta, String data, double valor) {

        UUID uuid = null;

        String[] returnedAttributes = {"id"};
        String sql = "insert into LANCAMENTOS_CONTABEIS (conta_contabil, data, valor)" +
                "values (?,?,?)";
        try (PreparedStatement insertStatement = jdbcTemplate.getDataSource().getConnection().prepareStatement(sql, returnedAttributes)) {
            insertStatement.setInt(1, conta);
            insertStatement.setString(2, data);
            insertStatement.setDouble(3, valor);
            int rows = insertStatement.executeUpdate();
            if (rows == 0) {
                    throw new SQLException("Failed of insertion");
            }
            try (ResultSet rs = insertStatement.getGeneratedKeys()) {
                if (rs.next()) {
                    uuid = (java.util.UUID) rs.getObject("id");
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }


        return uuid;
    }

    @Override
    public Lancamento getLancamentoById(UUID id) {
        Lancamento lancamento;
        String sql = "select * from LANCAMENTOS_CONTABEIS where id = ?";

        try {
            lancamento = jdbcTemplate.queryForObject(sql, new Object[]{id}, new LancamentoRowMapper());
        }catch (EmptyResultDataAccessException e){
            return  null;
        }

        return lancamento;
    }

    @Override
    public List<Lancamento> getLancamentoByConta(int conta) {
        String sql = "select * from LANCAMENTOS_CONTABEIS where CONTA_CONTABIL = ?";
        List<Lancamento> list = jdbcTemplate.query(sql, new Object[] { conta } , new LancamentoRowMapper());
        return list;
    }

    @Override
    public LancamentoStats getAllStats() {
        LancamentoStats lancamento;

        String sql = "select sum(valor) as soma, min(valor) as minimo, max(valor) as maximo,avg(valor) " +
                "as media,count(*) as quantidade from lancamentos_contabeis";
        try{
            lancamento = jdbcTemplate.queryForObject(sql, new LancamentoStatsRowMapper());
        }catch (EmptyResultDataAccessException e){
            return null;
        }



        return lancamento;
    }

    @Override
    public LancamentoStats getAllStatsByAccount(int conta) {
        LancamentoStats lancamento;
        String sql = "select sum(valor) as soma, min(valor) as minimo, max(valor) as maximo,avg(valor) " +
                "as media,count(*) as quantidade from lancamentos_contabeis where CONTA_CONTABIL = ?";
        try{
            lancamento = jdbcTemplate.queryForObject(sql, new Object[] { conta } , new LancamentoStatsRowMapper());
        }catch (EmptyResultDataAccessException e){
            return null;
        }
        return lancamento;
    }
}
