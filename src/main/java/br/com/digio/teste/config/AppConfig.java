package br.com.digio.teste.config;

import br.com.digio.teste.repository.LancamentoContabilRepository;
import br.com.digio.teste.repository.LancamentoContabilRepositoryImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories("br.com.digio.teste.repository")
public class AppConfig {

    public @Bean LancamentoContabilRepository repository(){
        return new LancamentoContabilRepositoryImpl() ;
    }
}
