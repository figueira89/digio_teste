package br.com.digio.teste;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;

import static io.restassured.RestAssured.given;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@RunWith(SpringRunner.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class DigioTesteApplicationTests {


    @Before
    public void init(){
        RestAssured.baseURI = "http://localhost";
        RestAssured.port = 8080;
    }

    @Test
    public void status200Insert() {
        RequestSpecification request = given();

        JSONObject requestParams = new JSONObject();
        try {
            requestParams.put("contaContabil", "1111542");
            requestParams.put("data", "20170130");
            requestParams.put("valor", 17347.00);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        request.header("Content-Type", "application/json");
        request.body(requestParams.toString());
        Response response = request.post("/lancamentos-contabeis");

        int statusCode = response.getStatusCode();
        Assert.assertEquals(statusCode, HttpStatus.CREATED.value());
    }

    @Test
    public void status400Insert() {
        RequestSpecification request = given();

        JSONObject requestParams = new JSONObject();

        request.header("Content-Type", "application/json");
        request.body(requestParams.toString());
        Response response = request.post("/lancamentos-contabeis");

        int statusCode = response.getStatusCode();
        Assert.assertEquals(statusCode, HttpStatus.BAD_REQUEST.value());
    }

    @Test
    public void status200getById() {
        RequestSpecification request = given();

        JSONObject requestParams = new JSONObject();
        try {
            requestParams.put("contaContabil", "1111542");
            requestParams.put("data", "20170130");
            requestParams.put("valor", 17347.00);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        request.header("Content-Type", "application/json");
        request.body(requestParams.toString());

        String id = request
                .contentType("application/json")
                .body(requestParams.toString())
                .when().post("/lancamentos-contabeis").then()
                .extract().path("id");

        given().when().get("/lancamentos-contabeis/"+id).then()
                .statusCode(200);


    }

    @Test
    public void status404getById() {
        RequestSpecification request = given();

        JSONObject requestParams = new JSONObject();
        try {
            requestParams.put("contaContabil", "1111542");
            requestParams.put("data", "20170130");
            requestParams.put("valor", 17347.00);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        request.header("Content-Type", "application/json");
        request.body(requestParams.toString());

        String id = request
                .contentType("application/json")
                .body(requestParams.toString())
                .when().post("/lancamentos-contabeis").then()
                .extract().path("id");

        given().when().get("/lancamentos-contabeis/"+UUID.randomUUID()).then()
                .statusCode(404);


    }

    @Test
    public void status200getByAccount() {
        RequestSpecification request = given();

        JSONObject requestParams = new JSONObject();
        try {
            requestParams.put("contaContabil", "1111542");
            requestParams.put("data", "20170130");
            requestParams.put("valor", 17347.00);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        request.header("Content-Type", "application/json");
        request.body(requestParams.toString());

         request
                .contentType("application/json")
                .body(requestParams.toString())
                .when().post("/lancamentos-contabeis");

        try {
            given().when().get("/lancamentos-contabeis/?contaContabil="+requestParams.get("contaContabil")).then()
                    .statusCode(200);
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    @Test
    public void status404getByAcount() {
        RequestSpecification request = given();

        JSONObject requestParams = new JSONObject();
        try {
            requestParams.put("contaContabil", "1111542");
            requestParams.put("data", "20170130");
            requestParams.put("valor", 17347.00);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        request.header("Content-Type", "application/json");
        request.body(requestParams.toString());

        request
                .contentType("application/json")
                .body(requestParams.toString())
                .when().post("/lancamentos-contabeis");

        given().when().get("/lancamentos-contabeis/?contaContabil="+0).then()
                .statusCode(404);


    }

    @Test
    public void status200getAllStats() {
        RequestSpecification request = given();

        JSONObject requestParams = new JSONObject();
        try {
            requestParams.put("contaContabil", "1111542");
            requestParams.put("data", "20170130");
            requestParams.put("valor", 17347.00);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        request.header("Content-Type", "application/json");
        request.body(requestParams.toString());

        request
                .contentType("application/json")
                .body(requestParams.toString())
                .when().post("/lancamentos-contabeis");

        try {
            given().when().get("/lancamentos-contabeis/stats").then()
                    .statusCode(200);
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    @Test
    public void status404getAllStats() {

        given().body("");
        given().when().get("/lancamentos-contabeis/stats").then()
                .statusCode(404);
    }

    @Test
    public void status200getAllStatsByAccount() {
        RequestSpecification request = given();

        JSONObject requestParams = new JSONObject();
        try {
            requestParams.put("contaContabil", "1111542");
            requestParams.put("data", "20170130");
            requestParams.put("valor", 17347.00);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        request.header("Content-Type", "application/json");
        request.body(requestParams.toString());

        request
                .contentType("application/json")
                .body(requestParams.toString())
                .when().post("/lancamentos-contabeis");

        try {
            given().when().get("/lancamentos-contabeis/stats/?contaContabil="+requestParams.get("contaContabil")).then()
                    .statusCode(200);
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    @Test
    public void status404getAllStatsByAccount() {
        RequestSpecification request = given();

        given().when().get("/lancamentos-contabeis/stats/?contaContabil="+0).then()
                .statusCode(404);
    }
}
